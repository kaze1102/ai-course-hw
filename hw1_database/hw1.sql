DROP DATABASE COLLEGE;

-- 1.
# a.
CREATE DATABASE COLLEGE;
USE COLLEGE;

CREATE TABLE STUDENT(
    StudentNumber       INTEGER         PRIMARY KEY,
    Name                VARCHAR(30)     NOT NULL,
    Class               INTEGER         NOT NULL,
    Major               VARCHAR(30)     NOT NULL
);

CREATE TABLE COURSE(
    CourseNumber        VARCHAR(10)     PRIMARY KEY,
    CourseName          VARCHAR(30)     NOT NULL,
    CreditHours         INTEGER         NOT NULL,
    Department          VARCHAR(30)     NOT NULL
);

CREATE TABLE SECTION(
    SectionIdentifier   VARCHAR(10)     PRIMARY KEY,
    CourseNumber        VARCHAR(30)     NOT NULL,
    Semester            CHAR(6)         NOT NULL,
    Year                YEAR            NOT NULL,
    Instructor          VARCHAR(30)     NOT NULL,
    FOREIGN KEY(CourseNumber) REFERENCES COURSE(CourseNumber)
        ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE GRADE_REPORT(
    StudentNumber       INTEGER,
    SectionIdentifier   VARCHAR(10),
    Grade               INTEGER,
    PRIMARY KEY(StudentNumber, SectionIdentifier),
    FOREIGN KEY(StudentNumber) REFERENCES STUDENT(StudentNumber)
        ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY(SectionIdentifier) REFERENCES SECTION(SectionIdentifier)
        ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE PREREQUISITE(
    CourseName          VARCHAR(10),
    PrequisiteNumber    VARCHAR(10),
    PRIMARY KEY(CourseName, PrequisiteNumber),
    FOREIGN KEY(CourseName) REFERENCES COURSE(CourseNumber)
        ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY(PrequisiteNumber) REFERENCES COURSE(CourseNumber)
        ON DELETE CASCADE ON UPDATE CASCADE
);

# b.
INSERT INTO STUDENT VALUES
(25, 'Johnson', 1, 'Math');

# c.
DELETE FROM STUDENT
WHERE Name = 'Smith' AND StudentNumber = 17;

# d.
UPDATE STUDENT
SET Class = 2
WHERE Name = 'Smith';

# e.
SELECT StudentNumber, Name
FROM STUDENT
WHERE Major = 'CSIE';

# f.
SELECT CourseName
FROM COURSE NATURAL JOIN SECTION
WHERE Instructor = 'Kingston' AND (Year = 2003 OR Year = 2004);

# g.
SELECT CourseNumber, Semester, Year, COUNT(StudentNumber)
FROM SECTION NATURAL JOIN GRADE_REPORT
WHERE Instructor = 'Kingston'
GROUP BY SectionIdentifier;

# h.
SELECT Name, CourseNumber, CourseName, CreditHours, Semester, Year, Grade
FROM STUDENT NATURAL JOIN GRADE_REPORT NATURAL JOIN SECTION NATURAL JOIN COURSE
WHERE Class = 5 AND Major = 'CSIE';

# i.
-- SELECT Name, Major
-- FROM STUDENT
-- WHERE StudentNumber IN (
--     SELECT DISTINCT(StudentNumber)
--     FROM GRADE_REPORT
--     WHERE Grade < 60
-- )
-- ORDER BY StudentNumber;
SELECT DISTINCT Name, Major
FROM STUDENT NATURAL JOIN GRADE_REPORT
WHERE Grade < 60
ORDER BY StudentNumber;

# j.
SELECT Name, AVG(Grade)
FROM STUDENT NATURAL JOIN GRADE_REPORT NATURAL JOIN SECTION
WHERE Year = 2019
GROUP BY StudentNumber
HAVING AVG(Grade) > 80;

-- 2.
# a.
SELECT sid, name, birthday
FROM Student NATURAL JOIN Classes
WHERE c_no = 'CS203';

# b.
SELECT DISTINCT c_no, title
FROM Courses NATURAL JOIN Classes
WHERE eid = 'E002'

# c.
SELECT c_no, title, credit
FROM Student NATURAL JOIN Classes NATURAL JOIN Courses
GROUP BY c_no
HAVING COUNT(sid) > 3;

# d.
SELECT sid, name, AVG(grade)
FROM Student NATURAL JOIN Classes
GROUP BY sid
ORDER BY sid

# e.
SELECT DISTINCT eid, name, rank
FROM Instructor NATURAL JOIN Classes
WHERE c_no = 'CS213'

# f.
SELECT SUM(credits)
FROM Courses NATURAL JOIN Classes
WHERE sid = 'S001'
GROUP by sid
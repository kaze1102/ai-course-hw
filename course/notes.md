# Introduction

* [Computational Thinking](http://tech-insider.org/academia/research/acrobat/0603.pdf)
* [Python cheat sheet](https://perso.limsi.fr/pointal/_media/python:cours:mementopython3-english.pdf)
---
# Keywords

* biomorphic system: a system run base on nature?
* cvpr(Computer Vision and Pattern Recognition) <- a paper collection
* julia: a new programming language (very fast?)
    - [programming speed compare](https://modelingguru.nasa.gov/docs/DOC-2625)
---


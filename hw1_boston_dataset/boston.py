import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_boston
from sklearn.linear_model import LinearRegression

def linearTest(x, y):
    x = np.array(x).reshape(len(x), 1)
    y = np.array(y).reshape(len(y), 1)
    clf = LinearRegression()
    clf.fit(x, y)
    pre = clf.predict(x)

    plt.scatter(x, y, s = 100)
    plt.plot(x, pre, "r-", linewidth=4)
    for idx, m in enumerate(x):
         plt.plot([m, m], [y[idx], pre[idx]], "g-")
    plt.show()
    print("coefficient", clf.coef_)
    print("intercept", clf.intercept_)
    print("Mean square error: ", np.mean(y-pre)**2)

def linearBoston():
    boston = load_boston()
    x = [n[12] for n in boston.data]
    y = [n for n in boston.target]
    linearTest(x, y)

if __name__ == '__main__':
    linearBoston()
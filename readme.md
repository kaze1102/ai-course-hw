# Introduction
---
This is a repository for the PEC AI course
# envirnment
---
* windows ubuntu bash

# dependencies
---
* python3
* pip
* virtualenv

# Setup
---
1. `virtualenv -p python3 env`
2. `source env/bin/activate`
3. `pip3 install -r requirement.txt`
4. `jupyter notebook`
5. open the uri on command line